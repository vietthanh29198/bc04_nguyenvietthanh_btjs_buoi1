/**
 *input:
 *input1 = 12
 *input2 = 24
 *
 *todo:
 *donVi = input1 % 10 = 2
 *
 *chuc = Math.floor(input1 / 10)
 *
 *
 * output:
 * result1: 1 + 2 = 3
 * result2: 2 + 4 = 6
 *
 */

// input1
var donVi, chuc;
var input1 = 12;
var input2 = 24;

donVi = input1 % 10;
chuc = Math.floor(input1 / 10);

var result1 = donVi + chuc;
console.log("result = ", result1);

// input2
donVi = input2 % 10;
chuc = Math.floor(input2 / 10);

var result2 = donVi + chuc;
console.log(" result2: ", result2);
