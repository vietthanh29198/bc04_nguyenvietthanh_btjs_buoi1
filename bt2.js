/**
 * input:5 so thuc
 * a = 3, b = 4, c = 8, d = 10, e = 20
 *
 * todo:
 * gia tri trung binh = (a + b + c + d + e):5
 *
 *
 * output:
 * average = 9
 *
 */

var a = 3,
  b = 4,
  c = 8,
  d = 10,
  e = 20;
var average = (a + b + c + d + e) / 5;
console.log("average: ", average);
