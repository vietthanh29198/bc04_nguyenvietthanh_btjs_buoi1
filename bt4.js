/**
 * input:
 * chieuDai = 8
 * chieuRong = 3
 *
 * todo:
 * Tinh dien tich HCN = chieuDai * chieuRong
 * chu vi = (chieuDai + chieuRong) * 2
 *
 * output:
 * dienTichHCN = 24
 * chuViHCN = 22
 *
 */

var chieuDai = 8;
var chieuRong = 3;
var dienTichHCN = chieuDai * chieuRong;
var chuViHCN = (chieuDai + chieuRong) * 2;
console.log("dienTichHCN =", dienTichHCN);
console.log("chuViHCN =", chuViHCN);
